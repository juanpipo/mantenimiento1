module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./componentes/ClaseComponent.js":
/*!***************************************!*\
  !*** ./componentes/ClaseComponent.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "C:\\React\\ReactCalculadora\\componentes\\ClaseComponent.js";

/* harmony default export */ __webpack_exports__["default"] = (function (props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    id: "clase-container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    __self: this
  }, "Clase de ", props.nombre), props.clase.map(function (item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "clase-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 7
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: item.foto,
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9
      },
      __self: this
    }, item.first_name, " ", item.last_name));
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("style", {
    js: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, "\n                        #clase-container {\n                            perspective:500px;\n                        }\n                        .clase-list {\n                            display:flex;\n                        }\n                        .clase-list img {\n                            padding:3px;\n                            border:1px solid #acc\n                        }\n                        .clase-list p {\n                            font-size:14px;\n                            font-weight:bold;\n                            font-style:italic;\n                            margin-left:12px;\n                        }\n                        .clase-list:hover {\n                            transform:tanslateY(10px)scale(1);\n                        }\n                       \n                    "));
});

/***/ }),

/***/ "./componentes/comunes/BarraTitulo.js":
/*!********************************************!*\
  !*** ./componentes/comunes/BarraTitulo.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "C:\\React\\ReactCalculadora\\componentes\\comunes\\BarraTitulo.js";


var links = [{
  url: "/",
  nombre: "Home"
}, {
  url: "/calc",
  nombre: "Calculadora"
}];
/* harmony default export */ __webpack_exports__["default"] = (function () {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "nav-wrapper",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#",
    className: "brand-logo",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, "Curso de Next.js y React"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    id: "nav-mobile",
    className: "right hide-on-med-and-down",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, links.map(function (link) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
      href: link.url,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, link.nombre)));
  }))));
});

/***/ }),

/***/ "./componentes/comunes/Header.js":
/*!***************************************!*\
  !*** ./componentes/comunes/Header.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "C:\\React\\ReactCalculadora\\componentes\\comunes\\Header.js";


/* harmony default export */ __webpack_exports__["default"] = (function () {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("head", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, "Mi curso de Next.js y React"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "viewport",
    content: "width=device-width,initial-sclae=1.0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("link", {
    rel: "stylesheet",
    href: "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("script", {
    src: "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("script", {
    src: "https://code.jquery.com/jquery-3.3.1.js",
    integrity: "sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=",
    crossorigin: "anonymous",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  })));
});

/***/ }),

/***/ "./data/estudiantes.js":
/*!*****************************!*\
  !*** ./data/estudiantes.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

var matematicas = [{
  "id": 1,
  "first_name": "Sadye",
  "last_name": "Sinnatt",
  "email": "ssinnatt0@harvard.edu",
  "gender": "Female",
  "foto": "https://robohash.org/aperiamexmodi.png?size=50x50&set=set1"
}, {
  "id": 2,
  "first_name": "Berget",
  "last_name": "Formby",
  "email": "bformby1@washingtonpost.com",
  "gender": "Female",
  "foto": "https://robohash.org/etcorruptinobis.bmp?size=50x50&set=set1"
}, {
  "id": 3,
  "first_name": "Andriette",
  "last_name": "Doale",
  "email": "adoale2@merriam-webster.com",
  "gender": "Female",
  "foto": "https://robohash.org/eosetexercitationem.jpg?size=50x50&set=set1"
}, {
  "id": 4,
  "first_name": "Pepe",
  "last_name": "Iacovo",
  "email": "piacovo3@t-online.de",
  "gender": "Male",
  "foto": "https://robohash.org/iddelenitiqui.bmp?size=50x50&set=set1"
}, {
  "id": 5,
  "first_name": "Estevan",
  "last_name": "Jarred",
  "email": "ejarred4@homestead.com",
  "gender": "Male",
  "foto": "https://robohash.org/quaeratdoloresenim.bmp?size=50x50&set=set1"
}, {
  "id": 6,
  "first_name": "Archibold",
  "last_name": "Boddie",
  "email": "aboddie5@webeden.co.uk",
  "gender": "Male",
  "foto": "https://robohash.org/eligendilaborumet.jpg?size=50x50&set=set1"
}, {
  "id": 7,
  "first_name": "Etty",
  "last_name": "Tumbelty",
  "email": "etumbelty6@ft.com",
  "gender": "Female",
  "foto": "https://robohash.org/aliquidtotamvelit.jpg?size=50x50&set=set1"
}, {
  "id": 8,
  "first_name": "Yolanda",
  "last_name": "Corns",
  "email": "ycorns7@fc2.com",
  "gender": "Female",
  "foto": "https://robohash.org/doloremquefugaet.png?size=50x50&set=set1"
}, {
  "id": 9,
  "first_name": "Seward",
  "last_name": "Capaldo",
  "email": "scapaldo8@wp.com",
  "gender": "Male",
  "foto": "https://robohash.org/eaqueoditnon.bmp?size=50x50&set=set1"
}, {
  "id": 10,
  "first_name": "Aggi",
  "last_name": "Gremain",
  "email": "agremain9@yahoo.com",
  "gender": "Female",
  "foto": "https://robohash.org/officiaetdebitis.png?size=50x50&set=set1"
}];
var ciencias = [{
  "id": 1,
  "first_name": "Sadye",
  "last_name": "Sinnatt",
  "email": "ssinnatt0@harvard.edu",
  "gender": "Female",
  "foto": "https://robohash.org/aperiamexmodi.png?size=50x50&set=set1"
}, {
  "id": 2,
  "first_name": "Berget",
  "last_name": "Formby",
  "email": "bformby1@washingtonpost.com",
  "gender": "Female",
  "foto": "https://robohash.org/etcorruptinobis.bmp?size=50x50&set=set1"
}, {
  "id": 3,
  "first_name": "Andriette",
  "last_name": "Doale",
  "email": "adoale2@merriam-webster.com",
  "gender": "Female",
  "foto": "https://robohash.org/eosetexercitationem.jpg?size=50x50&set=set1"
}, {
  "id": 4,
  "first_name": "Pepe",
  "last_name": "Iacovo",
  "email": "piacovo3@t-online.de",
  "gender": "Male",
  "foto": "https://robohash.org/iddelenitiqui.bmp?size=50x50&set=set1"
}, {
  "id": 5,
  "first_name": "Estevan",
  "last_name": "Jarred",
  "email": "ejarred4@homestead.com",
  "gender": "Male",
  "foto": "https://robohash.org/quaeratdoloresenim.bmp?size=50x50&set=set1"
}, {
  "id": 6,
  "first_name": "Archibold",
  "last_name": "Boddie",
  "email": "aboddie5@webeden.co.uk",
  "gender": "Male",
  "foto": "https://robohash.org/eligendilaborumet.jpg?size=50x50&set=set1"
}, {
  "id": 7,
  "first_name": "Etty",
  "last_name": "Tumbelty",
  "email": "etumbelty6@ft.com",
  "gender": "Female",
  "foto": "https://robohash.org/aliquidtotamvelit.jpg?size=50x50&set=set1"
}, {
  "id": 8,
  "first_name": "Yolanda",
  "last_name": "Corns",
  "email": "ycorns7@fc2.com",
  "gender": "Female",
  "foto": "https://robohash.org/doloremquefugaet.png?size=50x50&set=set1"
}, {
  "id": 9,
  "first_name": "Seward",
  "last_name": "Capaldo",
  "email": "scapaldo8@wp.com",
  "gender": "Male",
  "foto": "https://robohash.org/eaqueoditnon.bmp?size=50x50&set=set1"
}, {
  "id": 10,
  "first_name": "Aggi",
  "last_name": "Gremain",
  "email": "agremain9@yahoo.com",
  "gender": "Female",
  "foto": "https://robohash.org/officiaetdebitis.png?size=50x50&set=set1"
}];
module.exports = {
  matematicas: matematicas,
  ciencias: ciencias
};

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _componentes_ClaseComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../componentes/ClaseComponent */ "./componentes/ClaseComponent.js");
/* harmony import */ var _componentes_comunes_Header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../componentes/comunes/Header */ "./componentes/comunes/Header.js");
/* harmony import */ var _data_estudiantes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/estudiantes */ "./data/estudiantes.js");
/* harmony import */ var _data_estudiantes__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_data_estudiantes__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _componentes_comunes_BarraTitulo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../componentes/comunes/BarraTitulo */ "./componentes/comunes/BarraTitulo.js");
var _jsxFileName = "C:\\React\\ReactCalculadora\\pages\\index.js";






/* harmony default export */ __webpack_exports__["default"] = (function () {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-4141296793",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_componentes_comunes_Header__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_componentes_comunes_BarraTitulo__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    style: {
      textAlign: "center"
    },
    className: "jsx-4141296793",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "Cursos del plan"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      display: "flex",
      justifyContent: "space-around"
    },
    className: "jsx-4141296793",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_componentes_ClaseComponent__WEBPACK_IMPORTED_MODULE_2__["default"], {
    nombre: "Matematicas",
    clase: _data_estudiantes__WEBPACK_IMPORTED_MODULE_4__["matematicas"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_componentes_ClaseComponent__WEBPACK_IMPORTED_MODULE_2__["default"], {
    nombre: "Ciencias",
    clase: _data_estudiantes__WEBPACK_IMPORTED_MODULE_4__["ciencias"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    styleId: "4141296793",
    css: "body{background:#ffca20;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxSZWFjdFxcUmVhY3RDYWxjdWxhZG9yYVxccGFnZXNcXGluZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWtCWSxBQUdtQyxtQkFDcEIiLCJmaWxlIjoiQzpcXFJlYWN0XFxSZWFjdENhbGN1bGFkb3JhXFxwYWdlc1xcaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2xhc2VDb21wb25lbnQgZnJvbSAnLi4vY29tcG9uZW50ZXMvQ2xhc2VDb21wb25lbnQnXHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi4vY29tcG9uZW50ZXMvY29tdW5lcy9IZWFkZXInXHJcbmltcG9ydCB7bWF0ZW1hdGljYXMsY2llbmNpYXN9IGZyb20gJy4uL2RhdGEvZXN0dWRpYW50ZXMnXHJcbmltcG9ydCBCYXJyYVRpdHVsbyBmcm9tICcuLi9jb21wb25lbnRlcy9jb211bmVzL0JhcnJhVGl0dWxvJ1xyXG5cclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCAoKT0+KFxyXG4gICAgPGRpdj5cclxuICAgICAgPEhlYWRlci8+XHJcbiAgICAgIDxCYXJyYVRpdHVsby8+XHJcbiAgICAgICAgPGgxIHN0eWxlPXt7dGV4dEFsaWduOlwiY2VudGVyXCJ9fT5DdXJzb3MgZGVsIHBsYW48L2gxPlxyXG4gICAgICAgIDxkaXYgc3R5bGU9e3tkaXNwbGF5OlwiZmxleFwiLCBqdXN0aWZ5Q29udGVudDpcInNwYWNlLWFyb3VuZFwifX0+XHJcbiAgICAgICAgICAgPENsYXNlQ29tcG9uZW50IG5vbWJyZT1cIk1hdGVtYXRpY2FzXCIgY2xhc2U9e21hdGVtYXRpY2FzfS8+XHJcbiAgICAgICAgICAgPENsYXNlQ29tcG9uZW50IG5vbWJyZT1cIkNpZW5jaWFzXCIgY2xhc2U9e2NpZW5jaWFzfS8+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPHN0eWxlIGpzeCBnbG9iYWw+XHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGBcclxuICAgICAgICAgICAgICBib2R5e1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDojZmZjYTIwO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIDwvc3R5bGU+XHJcbiAgICAgICAgXHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICBcclxuKVxyXG4iXX0= */\n/*@ sourceURL=C:\\React\\ReactCalculadora\\pages\\index.js */",
    __self: this
  }));
});

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./pages/index.js */"./pages/index.js");


/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/link":
/*!****************************!*\
  !*** external "next/link" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map